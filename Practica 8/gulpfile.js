import gulp from 'gulp';
import dartSass from 'sass';
import gulpSass from 'gulp-sass';

import autoprefixer from 'gulp-autoprefixer';
import cleanCSS from 'gulp-clean-css';
import concat from 'gulp-concat';
import uglify from 'gulp-uglify';
import imagemin from 'gulp-imagemin';
import inject from 'gulp-inject';
import browserSync from 'browser-sync';

const sass = gulpSass(dartSass);

browserSync.create();

gulp.task('sass', function () {
  return gulp
    .src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(cleanCSS({ compatibility: 'ie8' }))
    .pipe(concat('style.css'))
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('js', function () {
  return gulp
    .src('./js/*.js')
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('images', function () {
  return gulp.src('./img/*').pipe(imagemin()).pipe(gulp.dest('./dist/img'));
});

gulp.task('inject', function () {
  return gulp
    .src('./index.html')
    .pipe(
      inject(gulp.src(['./css/style.css', './js/app.js'], { read: false }), {
        relative: true,
      })
    )
    .pipe(gulp.dest('./dist'));
});

gulp.task('build', gulp.series('sass', 'js', 'images', 'inject'));

gulp.task(
  'serve',
  gulp.series('build', function () {
    browserSync.init({
      server: './dist',
    });

    gulp.watch('./css/*.scss', gulp.series('sass'));
    gulp.watch('./js/*.js', gulp.series('js'));
    gulp.watch('./img/*', gulp.series('images'));
    gulp.watch('./index.html', gulp.series('inject'));
    gulp.watch('./dist/**/*').on('change', browserSync.reload);
  })
);
