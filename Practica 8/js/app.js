const button = document.getElementById('myButton');
const count = document.getElementById('count');
let clicks = 0;

button.addEventListener('click', function () {
  clicks += 1;
  count.innerHTML = clicks;
});
