const screenField = document.getElementById('screen');
const buttons = document.querySelectorAll('button');

function addition(firstOperand, secondOperand) {
  const result = parseFloat(firstOperand) + parseFloat(secondOperand);
  return result;
}

function subtraction(firstOperand, secondOperand) {
  const result = parseFloat(firstOperand) - parseFloat(secondOperand);
  return result;
}

function multiplication(firstOperand, secondOperand) {
  const result = parseFloat(firstOperand) * parseFloat(secondOperand);
  return result;
}

function division(firstOperand, secondOperand) {
  const result = parseFloat(firstOperand) / parseFloat(secondOperand);
  return result;
}

// Adding click event listeners to the buttons
buttons.forEach((button) => {
  switch (button.id) {
    case 'one':
    case 'two':
    case 'three':
    case 'four':
    case 'five':
    case 'six':
    case 'seven':
    case 'eight':
    case 'nine':
    case 'zero':
      button.addEventListener('click', function () {
        screenField.value += button.textContent;
      });
      break;

    case 'plus':
      button.addEventListener('click', function () {
        screenField.value += '+';
      });
      break;
    case 'minus':
      button.addEventListener('click', function () {
        screenField.value += '-';
      });
      break;
    case 'times':
      button.addEventListener('click', function () {
        screenField.value += '×';
      });
      break;
    case 'divide':
      button.addEventListener('click', function () {
        screenField.value += '÷';
      });
      break;
    case 'clear':
      button.addEventListener('click', function () {
        screenField.value = '';
      });
      break;
    case 'equal':
      button.addEventListener('click', function () {
        let result;
        if (screenField.value.includes('+')) {
          const operandsAdd = screenField.value.split('+');
          result = addition(operandsAdd[0], operandsAdd[1]);
        } else if (screenField.value.includes('-')) {
          const operandsSub = screenField.value.split('-');
          result = subtraction(operandsSub[0], operandsSub[1]);
        } else if (screenField.value.includes('×')) {
          const operandsMult = screenField.value.split('×');
          result = multiplication(operandsMult[0], operandsMult[1]);
        } else if (screenField.value.includes('÷')) {
          const operandsDiv = screenField.value.split('÷');
          result = division(operandsDiv[0], operandsDiv[1]);
        }
        screenField.value = result;
      });
      break;
  }
});

module.exports = { addition, subtraction, multiplication, division };
