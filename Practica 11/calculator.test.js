const { addition, subtraction, multiplication, division } = require('./app');

const screenField = document.getElementById('screen');
const buttons = document.querySelectorAll('button');

// const jsdom = require('jsdom');
// const { JSDOM } = jsdom;

// global.document = jsdom.window.document;
// global.window = jsdom.window;

// const dom = new JSDOM(`<!DOCTYPE html>
// <html>

// <head>
//   <title>Calculator</title>
//   <link rel="stylesheet" href="style.css">
//   <script src="./app.js" defer></script>
// </head>

// <body>
//   <div class="calculator">
//     <input type="text" id="screen" disabled/>
//     <br />
//     <br />
//     <div class="buttons">
//       <button id="seven">7</button>
//       <button id="eight">8</button>
//       <button id="nine">9</button>
//       <button id="plus">+</button>
//       <button id="four">4</button>
//       <button id="five">5</button>
//       <button id="six">6</button>
//       <button id="minus">-</button>
//       <button id="one">1</button>
//       <button id="two">2</button>
//       <button id="three">3</button>
//       <button id="times">&times;</button>
//       <button id="zero">0</button>
//       <button id="point">.</button>
//       <button id="divide">&divide;</button>
//       <button id="clear">C</button>
//     </div>
//       <br>
//       <button id="equal">=</button>
//     </div>
//   </body >
// </html >`);

describe('Calcutaror basic operations', () => {
  test('sum calculator', () => {
    expect(addition(2, 1)).toBe(3);
  });

  test('substract calculator', () => {
    expect(subtraction(4, 2)).toBe(2);
    expect(subtraction(2, 4)).toBe(-2);
  });

  test('multiply calculator', () => {
    expect(multiplication(5, 5)).toBe(25);
    expect(multiplication(5, -5)).toBe(-25);
  });

  test('divide calculator', () => {
    expect(division(4, 2)).toBe(2);
    expect(division(2, 4)).toBe(0.5);
    expect(division(2, 0)).toBe(Infinity);
  });
});

describe('Calculator screen and buttons', () => {
  let screenField;
  beforeEach(() => {
    document.body.innerHTML = `
      <body>
        <div class="calculator">
          <input type="text" id="screen" disabled />
        </div>
      </body>
    `;
    screenField = document.getElementById('screen');
  });

  test('Screen field should update its value when a number button is clicked', () => {
    buttons.forEach((button) => {
      if (button.textContent >= '0' && button.textContent <= '9') {
        button.dispatchEvent(new MouseEvent('click'));
        expect(screenField.value).toBe(button.textContent);
        screenField.value = '';
      }
    });
  });

  test('Screen field should update its value with the operation sign when an operation button is clicked', () => {
    const operations = ['+', '-', '×', '÷'];
    buttons.forEach((button) => {
      if (operations.includes(button.textContent)) {
        button.dispatchEvent(new MouseEvent('click'));
        expect(screenField.value).toBe(button.textContent);
        screenField.value = '';
      }
    });
  });

  test('Screen field should be emptied when the clear button is clicked', () => {
    screenField.value = '123456';
    buttons.forEach((button) => {
      if (button.textContent === 'C') {
        button.dispatchEvent(new MouseEvent('click'));
        expect(screenField.value).toBe('');
      }
    });
  });
});

describe('click event listeners', () => {
  let screenField;
  beforeEach(() => {
    document.body.innerHTML = `
      <body>
        <div class="calculator">
          <input type="text" id="screen" disabled />
        </div>
      </body>
    `;
    screenField = document.getElementById('screen');
  });

  beforeEach(() => {
    screenField.value = '';
  });

  test('displays input from number buttons on screen', () => {
    document.body.innerHTML += `
      <button id="seven">7</button>
      <button id="eight">8</button>
      <button id="nine">9</button>
    `;

    const sevenButton = document.getElementById('seven');
    sevenButton.dispatchEvent(new Event('click'));
    screenField.value += sevenButton.textContent;
    expect(screenField.value).toBe('7');

    const eightButton = document.getElementById('eight');
    eightButton.dispatchEvent(new Event('click'));
    screenField.value += eightButton.textContent;
    expect(screenField.value).toBe('78');

    const nineButton = document.getElementById('nine');
    screenField.value += nineButton.textContent;
    nineButton.dispatchEvent(new Event('click'));
    expect(screenField.value).toBe('789');
  });
});
