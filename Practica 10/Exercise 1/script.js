let weather = {
  apiKey: '7889565148a7f26fbdca2ad90300c0b0',
  fetchWeather: function (city) {
    fetch(
      'https://api.openweathermap.org/data/2.5/weather?q=' +
        city +
        '&units=metric&appid=' +
        this.apiKey
    )
      .then((response) => {
        this.hideLoadingSpinner();
        return response.json();
      })
      .then((data) => this.displayWeather(data));
  },
  displayWeather: function (data) {
    const { name } = data;
    const { icon, description } = data.weather[0];
    const { temp, humidity } = data.main;
    const { speed } = data.wind;
    document.querySelector('.city').innerText = name;
    document.querySelector('.icon').src =
      'https://openweathermap.org/img/wn/' + icon + '.png';
    document.querySelector('.description').innerText = description;
    document.querySelector('.temp').innerText = temp + '°C';
    document.querySelector('.humidity').innerText =
      'Humedad: ' + humidity + '%';
    document.querySelector('.wind').innerText = 'Viento a: ' + speed + ' km/h';
    document.querySelector('.precip').innerText =
      'Prob. de Precipitaciones: ' + (humidity - 40) + '%';
    document.querySelector('.weather').classList.remove('loading');
  },
  //get content from selection
  search: function () {
    this.fetchWeather(document.querySelector('.city-select').value);
  },
  showLoading: function () {
    document.getElementById('loader').style.display = 'block';
  },
  hideLoadingSpinner: function () {
    document.getElementById('loader').style.display = 'none';
  },
};

document.querySelector('.city-select').addEventListener('change', function () {
  weather.search();
  weather.showLoading();
});

weather.fetchWeather('La Paz');
