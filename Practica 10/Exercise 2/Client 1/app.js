const ws = new WebSocket('ws://localhost:8082');
ws.addEventListener('open', () => {
  console.log('We are connected');
});

ws.addEventListener('message', ({ data }) => {
  if (data instanceof Blob) {
    reader = new FileReader();

    reader.onload = () => {
      console.log('Result: ' + reader.result);
      const newReceivedMessage = document.createElement('p');
      newReceivedMessage.innerText = reader.result;
      chatBody.appendChild(newReceivedMessage);
    };

    reader.readAsText(data);
  } else {
    console.log('Result: ' + data);
  }
});

const chatContainer = document.querySelector('.chat-container');
const chatButton = document.querySelector('.chat-button');
const closeBtn = document.querySelector('.close-btn');
const chatBody = document.querySelector('.chat-body');
const messageInput = document.querySelector("input[type='text']");
const messageForm = document.querySelector('form');

chatButton.addEventListener('click', function () {
  chatContainer.style.display = 'flex';
});

closeBtn.addEventListener('click', function () {
  chatContainer.style.display = 'none';
});

messageForm.addEventListener('submit', (e) => {
  e.preventDefault();
  const newMessage = document.createElement('p');
  newMessage.innerText = messageInput.value;
  let message = {
    id: '1',
    content: messageInput.value,
  };
  ws.send(JSON.stringify(message));
  messageInput.value = '';
});
