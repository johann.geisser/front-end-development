const WebSocket = require('ws');

const wss = new WebSocket.Server({ port: 8082 });

wss.on('connection', (ws) => {
  console.log('New client connected');

  ws.on('message', (data) => {
    console.log(`Client has sent us: ${data}`);
    wss.broadcast(data);
  });

  wss.broadcast = function broadcast(msg) {
    console.log(msg);
    wss.clients.forEach(function each(client) {
      client.send(msg);
    });
  };

  ws.on('close', () => {
    console.log('Client has disconnected!');
  });
});
