let form = document.getElementById('reservationForm');

let inputel = document.getElementById('ss');

let data = localStorage.getItem('reservations');
console.log(JSON.parse(data));

inputel.addEventListener('click', (event) => {
  let arrivalDate = document.getElementById('arrival-date').value;
  let nights = document.getElementById('nights').value;
  let adults = document.getElementById('adults').value;
  let children = document.getElementById('children').value;

  let roomType;
  let bedType;

  let roomTypes = document.querySelectorAll('input[name=room-type]');
  roomTypes.forEach((input) => {
    if (input.checked) {
      roomType = input.value;
    }
  });

  let bedTypes = document.querySelectorAll('input[name=bed-type]');
  bedTypes.forEach((input) => {
    if (input.checked) {
      bedType = input.value;
    }
  });
  let smoke = document.getElementById('smoke').checked;

  let name = document.getElementById('name').value;
  let email = document.getElementById('email').value;
  let phone = document.getElementById('phone').value;

  let formData = {
    arrivalDate,
    nights,
    adults,
    children,
    roomType,
    bedType,
    smoke,
    name,
    email,
    phone,
  };

  let reservations = JSON.parse(data);
  reservations.push(formData);

  localStorage.setItem('reservations', JSON.stringify(reservations));

  console.log(JSON.parse(localStorage.getItem('reservations')));
});
