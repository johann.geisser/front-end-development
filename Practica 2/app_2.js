const tbody = document.getElementById('reservation-list');

function updateReservationTable() {
  const reservations = JSON.parse(localStorage.getItem('reservations')) || [];
  const tableBody = document.getElementById('reservation-table-body');
  tableBody.innerHTML = '';
  let i = 0;

  reservations.forEach((reservation) => {
    const newRow = document.createElement('tr');
    newRow.innerHTML = `
                <td>${reservation.arrivalDate}</td>
                <td>${reservation.nights}</td>
                <td>${reservation.adults}</td>
                <td>${reservation.children}</td>
                <td>${reservation.roomType}</td>
                <td>${reservation.bedType}</td>
                <td>${
                  reservation.smoke
                    ? '<img src="smoke-allowed.jpg" style="width: 20px; height: 20px; margin-left:10px">'
                    : '<img src="no-smoking.jpg" style="width: 20px; height: 20px; margin-left:10px">'
                }</td>
                <td>${reservation.name}</td>
                <td>${reservation.email}</td>
                <td>${reservation.phone}</td>
                <td onClick='clickeado(${i})'>Remove</td>
            `;
    tableBody.appendChild(newRow);
    i++;
  });
}

updateReservationTable();

function clickeado(reservationData) {
  if (
    window.confirm('Are you sure you want to delete this reservation') == true
  ) {
    let reservations = JSON.parse(localStorage.getItem('reservations')) || [];
    reservations.splice(reservationData, 1);
    localStorage.setItem('reservations', JSON.stringify(reservations));
    window.location.reload();
  }
}
