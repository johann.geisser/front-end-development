// function fibonacci(n) {
//   let arr = [0, 1, 1];
//   let i = 3;

//   if (n === 0) {
//     return [];
//   }

//   if (n === 1) {
//     return arr.pop();
//   }

//   while (i < n) {
//     arr.push(arr[i - 2] + arr[i - 1]);
//     i++;
//   }
//   return arr;
// }

function nBonacci(n, m) {
  if (n === 0 || m === 0) {
    return 0;
  }

  let arr = Array(m).fill(0);
  arr[n - 1] = 1;

  for (i = n; i < m; i++) {
    for (j = i - n; j < i; j++) {
      arr[i] += arr[j];
    }
  }
  return arr[arr.length - 1];
}

console.log(nBonacci(2, 5));
console.log(nBonacci(3, 7));
console.log(nBonacci(0, 0));
