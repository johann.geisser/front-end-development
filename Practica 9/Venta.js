function countCombinations(price, bankNote) {
  if (price === 0) {
    return 0;
  }
  let dp = Array(price + 1).fill(0);
  dp[0] = 1;

  for (let i = 0; i < bankNote.length; i++) {
    for (let j = bankNote[i]; j <= price; j++) {
      dp[j] += dp[j - bankNote[i]];
    }
  }
  return dp[price];
}

const bankNote = [1, 2, 5, 10, 20, 50, 100, 200];
const price = 5;
console.log(countCombinations(price, bankNote)); // 4
